terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}

#######
# VPC #
#######
# Create a VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}


###########
# SUBNETS #
###########

  resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main"
  }
}

############
# GATEWAYS #
############

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}



################
# ROUTE TABLES #
################

resource "aws_route_table" "worker" {
  count      = var.destroy == true ? 0 : 1
  vpc_id = aws_vpc.worker[0].id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.worker[0].id
  }
}

resource "aws_route_table_association" "worker" {
  count      = var.destroy == true ? 0 : 3
  subnet_id      = aws_subnet.worker[count.index].id
  route_table_id = aws_route_table.worker[0].id
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "10.0.1.0/24"
    gateway_id = aws_internet_gateway.gw.id
  }

resource "aws_security_group" "alb" {
  name        = "terraform_alb_security_group"
  description = "Terraform load balancer security group"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "terraform-example-alb-security-group"
  }
}

######################
# EC2 security group #
######################


resource "aws_security_group" "allow_acces_db" {
  name        = "allow_access_db"
  description = "Allow access to DB"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "llow access to DB"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.main.cidr_block]
    ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }
  resource "aws_rds_cluster" "my_db" {
    cluster_identifier        = "my_db"
    availability_zones        = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
    engine                    = "postgres"
    db_cluster_instance_class = "db.t2.nano"
    storage_type              = "io1"
    allocated_storage         = 100
    iops                      = 1000
    master_username           = "admin"
    master_password           = "1234"
  }

